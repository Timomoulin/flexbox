import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';


export default function App() {
  return (
    <SafeAreaView style={styles.container}>

      <View style={styles.vue, styles.vue1}> 
        <View style={styles.vueTitre}>
          <Text style={styles.titre}>Rubrique 1</Text>
        </View>
        <View style={styles.vueContenu1}>
        <Text style={styles.texte}> Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit quibusdam fugit, facere omnis expedita facilis repellendus veniam ad. Quo nulla beatae iste dolores quibusdam, illo sed! Incidunt molestiae sint tempora? </Text>
        </View>
      </View> 

      <View style={styles.vue, styles.vue2}>
        <View style={styles.vueTitre}>
          <Text style={styles.titre}>Rubrique 2</Text>
          <View style={styles.vueContenu2}>
            <View style={styles.conteneurTexte}>
              <Text style={styles.texte}> Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit quibusdam fugit, facere omnis expedita facilis repellendus veniam ad. Quo nulla beatae iste dolores quibusdam, illo sed! Incidunt molestiae sint tempora? </Text>
            </View>
            <View style={styles.conteneurTexte}>
              <Text style={styles.texte}>Labore dolor voluptatum id alias quo provident incidunt repudiandae sequi necessitatibus aliquid doloribus tempora saepe tempore, quam obcaecati distinctio delectus velit rem et non, nulla commodi impedit molestias nihil. A!</Text>
            </View>
        </View>
        </View>
      </View>

      <View style={styles.vue, styles.vue3}>
        <View style={styles.vueTitre}>
          <Text style={styles.titre}>Rubrique 3</Text>
        </View>
        <View style={styles.vueContenu3}>
          <View style={styles.conteneurImg}>
            <Image source={require('./assets/favicon.png')}
            style={styles.imageLorem}
            />
          </View>
          <View style={styles.conteneurTexte}>
          <Text style={styles.texte}>Labore issimos unde quas accusamus maiores repellendus officia eligendi velit iure tenetur.
          </Text>
          </View>
        </View>
      </View>

      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#fff',
    justifyContent:'space-around',
    width:"95%",
    alignSelf:'center'
  },
 
  vue1:{
    
    backgroundColor:'#2C4770',
    borderWidth:3,
    borderColor:'#000000',
  
  },
  vue2:{
    backgroundColor:'#AA9A39',
    borderWidth:3,
    borderColor:'#000000',
    
  },
  vue3:{
    backgroundColor:'#74276D',
    borderWidth:3,
    borderColor:'#000000',
   
  },
  vueTitre:{
    justifyContent:'center',
    alignItems:'center'
    
  },
  titre:{
    fontWeight:'bold',
    color:'#ffff',
    fontSize:26,
  },
  texte:{
    color:'#ffff',

  },
  vueContenu1:{
    margin:5,
  },
  vueContenu2:{
    margin:5,
  },
  vueContenu3:{
    margin:5
  },
  conteneurTexte:{
    
    marginHorizontal:5,
  },
  imageLorem:{
    resizeMode:'stretch',
  },
  conteneurImg:{
   
  }

});
